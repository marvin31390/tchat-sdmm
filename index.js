// on met en place mongoose / init mongoose
const mongoose = require('mongoose');
//on met en place le chemin vers la bdd / we set up the root to the db
const mongoDB = 'mongodb+srv://SDMM:AZERTYUIOP1234567890@tchatsdmm.krbxw.mongodb.net/tchatSDMM?retryWrites=true&w=majority'
//on connecte mongoose à mongodb / we connect mongoose to mongodb
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => {
	console.log('connecté a la base de donnée')
}).catch(err => console.log(err));

const Msg = require('./models/messages')

// on met en place Express / init Express
const express = require('express');

//on définit app comme la fonction express / we set app like express function
const app = express();

// on met en place mongodb / init mongodb
const { MongoClient } = require("mongodb");

// on utilise le dossier public / use public file
app.use(express.static('public'));

// on crée le serveur HTTP / create HTTP serveur
const http = require("http").createServer(app);

// on met en place socket.io / init socket.io
const io = require("socket.io")(http);

// Utilise le port de l'hébergeur ou 8080 / Use default port of heberg or 8080
const port = process.env.PORT || 8080


// on met en place la route "/" / we build the project root
app.get("/", (req, res) => {
	res.sendFile(__dirname + "/index.html")
})
// On demande au serveur http de se lancer sur le port 8080 / we ask http server listen 8080 port
http.listen(port, () => {
	console.log(`port ${port} en ligne / ${port} is online`);
});

// on ecoute l'evenement connexion de "socket.io" / we listen socket.io conection
io.on('connection', function (socket) {
	// chercher
	Msg.find().then(res => {
		socket.emit('output-messages', res)
	})

	//on crée une fonction qui envoi le message a bdd / we create submit function who send the message to the db
	socket.on('chat_message', function (msg) {
		const messageSave = new Msg({ msg });
		console.log('test', messageSave.msg.name)
		if (!messageSave.msg.name || typeof messageSave.msg.name == undefined || messageSave.msg.name.length > 25) {
			socket.emit('error message', "Le pseudonyme rentré n'est pas valide !");
			return;
		}

		// Vérification du message / message verif
		if (!messageSave.msg.message || typeof messageSave.msg.message == undefined || messageSave.msg.message.length > 255) {
			socket.emit('error message', "Le message rentré n'est pas valide !");
			return;
		}
		// on post le message / we post the message
		messageSave.save().then(() => {
			io.emit('received_message', msg);
		});
		// on ecoute les deconnexions / we listen the deconection
		socket.on('disconnect', function () {
			delete socket;
		});

		


	});


});
