// on se connecte au serveur socket / connection to socket server
const socket = io();
//on attend que la page soit chargée / we wait the loading of the page
window.onload = () => {
    // on recupere le form / we get form
    document.querySelector("form").addEventListener("submit", (e) => {
        // on empeche l'envoi du formulaire / we stop the form submit
        e.preventDefault();
        //on recupere l'input nom / we get the name input
        const name = document.querySelector('#name');
        //on recupere l'input message / we get the message input
        const message = document.querySelector('#message');
        // on met en place l'heure / we set up the date
        let heure = new Date();
        // on envoi le message / we send the message
        socket.emit("chat_message", { name: name.value, message: message.value, date: heure });
        //on efface l'input message apres l'envoi / we erase the message input after the send
        document.querySelector("#message").value = '';

    })
    // on ecoute l'evenement message reçu / we listen received message event
    socket.on("received_message", (msg) => {
        //on convertie la date du message en millisecondes / we convert the message date into milliseconds
        let dateUser = Date.parse(msg.date) / 1000;
        //on convertie la date actuelle en millisecondes / we convert the actuel date into milliseconds
        let dateNow = Date.now() / 1000;
        //on soustrais la date du message à la date actuelle en la transformant en minutes / we substrac the message date to the actual date into minutes
        let slashDate = Math.floor((dateNow - dateUser) / 60);
        
        let d = new Date();
        let date = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate();
        let hours = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

        if (slashDate <= 2)
        {
            slashDate = `à l'instant`;
        }
        else if (slashDate > 2 && slashDate <= 60)
        { 
            slashDate = `il y a ${Math.floor((dateNow - dateUser) / 60)} minutes`;
        }
        else if (slashDate > 60)
        {
            slashDate = `Aujourd'hui à ${hours}`;
        }
        else if (slashDate >= 3600)
        {
            slashDate = `${date}`;
        }
        
        // on crée une variable qui va elle meme crée une balise <p> pour afficher le pseudo, le message et la date / we create a variable that will itself create a <p> tag to display the nickname, the message and the date
        let createP=document.createElement('p');
        createP.innerHTML = `${slashDate}<br>${msg.name}: ${msg.message}`;
        // on recupere la div messages apres la création de la balise <p> / we take the message div after creating the tag <p>
        document.querySelector("#messages").append(createP);
        
    })
    // on ecoute l'evenement output-messages / we listen the submit output-messages
    socket.on('output-messages', (data) => {

        data.slice(data.length - 15, data.length).forEach(element => {
        //on convertie la date du message en millisecondes / we convert the message date into milliseconds
        let dateUser = Date.parse(element.msg.date) / 1000;
        //on convertie la date actuelle en millisecondes / we convert the actuel date into milliseconds
        let dateNow = Date.now() / 1000;
        //on soustrais la date du message à la date actuelle en la transformant en minutes / we substrac the message date to the actual date into minutes
        let slashDate = Math.floor((dateNow - dateUser) / 60);
    
        let d = new Date();
        let date = d.getDate() + '-' + (d.getMonth()+1) + '-' + (d.getFullYear()-1);
        let hours = d.getHours() + ":" + d.getMinutes();

        if(slashDate <= 2)
        {
            slashDate = `à l'instant`;
        }
        else if (slashDate > 2 && slashDate <= 60)
        { 
            slashDate = `il y a ${Math.floor((dateNow - dateUser) / 60)} minutes`;
        }
        else if (slashDate > 60 && slashDate <= 3600) 
        {
            slashDate = `Aujourd'hui à ${hours}`;
        }
        else if (slashDate > 3600)
        {
            slashDate = `Le ${date}`;
        }
        
        // on recupere la div messages pour y crée un element p / we take the message div for creating tag p
        document.querySelector("#messages").innerHTML += `<p>${slashDate} <br> ${element.msg.name} dit : ${element.msg.message}</p>`;


        });
        




    })

}



